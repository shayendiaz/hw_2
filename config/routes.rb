Rails.application.routes.draw do
  get '/' => 'posts#index', as: 'posts'
  get '/posts/:id' => 'posts#show', as: 'post'
  get '/authors' => 'authors#index', as: 'authors'
  get '/authors/:name' => 'authors#show', as: 'author'
end
