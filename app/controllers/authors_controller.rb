class AuthorsController < ApplicationController
  def index
  	@authors = [{name: "John Smith"}, {name: "Hugh Jass"}, {name: "Amanda Hugginkiss"}]
  end

  def show
  	name = params[:name]
  	@author = {name: name}
  end
end